//
//  Header.h
//  Sistema FixIt
//
//  Created by Luis Henrique Baster Massud on 07/11/14.
//  Copyright (c) 2014 Luique Works. All rights reserved.
//

#ifndef Sistema_FixIt_Header_h
#define Sistema_FixIt_Header_h

FILE *bkpcliente;

typedef struct t_endereco {
    char rua[51];
    char numero[51];
    char cep[51];
    char bairro[51];
    char estado[51];
    char cidade[51];
}tipo_endereco;

typedef struct tipo_cliente cadastro_cliente;
struct tipo_cliente {
    char nome[51];
    char rg[51];
    char telefone[51];
    tipo_endereco endereco;
    char email[51];
    int num_cliente;
    struct tipo_cliente *prox;
};


int erro;
int numero_cliente = 1;
cadastro_cliente info;
cadastro_cliente *ponteiro_clientes;


int Inicializar_Cliente (cadastro_cliente **inicio);
int Inserir_Cliente_Comeco (cadastro_cliente **inicio, char nome[51], char rg[51], char telefone[51], char email[51], char rua[51], char numero[51], char cep[51], char bairro[51], char estado[51], char cidade[51]);
int Inserir_Cliente_Meio (cadastro_cliente **inicio, char nome[51], char rg[51], char telefone[51], char email[51], char rua[51], char numero[51], char cep[51], char bairro[51], char estado[51], char cidade[51]);
int Listar_Cliente (cadastro_cliente *inicio);
int ordenar (cadastro_cliente **inicio, char nome[51]);
int Obter_Tamanho(cadastro_cliente **inicio);
int Remover_Cliente (cadastro_cliente **inicio, char nome[51]);
int Checar_Cliente (cadastro_cliente **inicio, char nome[51]);
int Gerar_Cadastro (cadastro_cliente **inicio, cadastro_cliente info);
void salvarBinario(cadastro_cliente **inicio);
void carregarBinario(cadastro_cliente **inicio);


int Gerar_Cadastro (cadastro_cliente **inicio, cadastro_cliente info){
    
    printf("*************CADASTRO DO CLIENTE*************\nNOME DO CLIENTE\n");
    getchar();
    fgets(info.nome, 51 , stdin);
    printf("\nRG DO CLIENTE\n");
    fgets(info.rg, 51 , stdin);
    printf("\nTELEFONE DO CLIENTE\n");
    fgets(info.telefone, 51, stdin);
    printf("\nEMAIL DO CLIENTE\n");
    fgets(info.email, 51, stdin);
    printf("\n*************ENDERECO DO CLIENTE*************\n");
    printf("\nRUA DO CLIENTE\n");
    fgets(info.endereco.rua, 51, stdin);
    printf("\nNUMERO DO CLIENTE\n");
    fgets(info.endereco.numero, 51, stdin);
    printf("\nCEP DO CLIENTE\n");
    fgets(info.endereco.cep, 51, stdin);
    printf("\nBAIRRO DO CLIENTE\n");
    fgets(info.endereco.bairro, 51, stdin);
    printf("\nESTADO DO CLIENTE\n");
    fgets(info.endereco.estado, 51, stdin);
    printf("\nCIDADE DO CLIENTE\n");
    fgets(info.endereco.cidade, 51, stdin);
    numero_cliente++;
    info.prox = NULL;
    info.num_cliente = numero_cliente;
    Inserir_Cliente_Meio(&ponteiro_clientes, info.nome, info.rg, info.telefone, info.email, info.endereco.rua, info.endereco.numero, info.endereco.cep, info.endereco.bairro, info.endereco.estado, info.endereco.cidade);
    
    if (erro == 0) printf("Insercao realizada com sucesso\n");
    
    return 0;
}



int Inicializar_Cliente (cadastro_cliente **inicio)
{
    *inicio= NULL;
    return 0;
}





int Inserir_Cliente_Comeco (cadastro_cliente **inicio, char nome[51], char rg[51], char telefone[51], char email[51], char rua[51], char numero[51], char cep[51], char bairro[51], char estado[51], char cidade[51]){
    
    cadastro_cliente *nonovo = NULL;
    nonovo = (cadastro_cliente *) malloc(sizeof(cadastro_cliente));
    strcpy(nonovo -> nome, nome);
    strcpy(nonovo -> rg, rg);
    strcpy(nonovo -> telefone, telefone);
    strcpy(nonovo -> email, email);
    strcpy(nonovo -> endereco.rua, rua);
    strcpy(nonovo -> endereco.numero, numero);
    strcpy(nonovo -> endereco.cep, cep);
    strcpy(nonovo -> endereco.bairro, bairro);
    strcpy(nonovo -> endereco.estado, estado);
    strcpy(nonovo -> endereco.cidade, cidade);
    if (*inicio==NULL)
    {
        nonovo -> prox = NULL;
        *inicio = nonovo;
    } else {
        nonovo -> prox = *inicio;
        *inicio = nonovo;
    }
    return 0;
}








int Inserir_Cliente_Meio (cadastro_cliente **inicio, char nome[51], char rg[51], char telefone[51], char email[51], char rua[51], char numero[51], char cep[51], char bairro[51], char estado[51], char cidade[51])
{
    int pos;
    int tam;
    tam = Obter_Tamanho(inicio);
    cadastro_cliente *nonovo, *percorre;
    
    nonovo = (cadastro_cliente *)malloc(sizeof(cadastro_cliente));
    strcpy(nonovo -> nome, nome);
    strcpy(nonovo -> rg, rg);
    strcpy(nonovo -> telefone, telefone);
    strcpy(nonovo -> email, email);
    strcpy(nonovo -> endereco.rua, rua);
    strcpy(nonovo -> endereco.numero, numero);
    strcpy(nonovo -> endereco.cep, cep);
    strcpy(nonovo -> endereco.bairro, bairro);
    strcpy(nonovo -> endereco.estado, estado);
    strcpy(nonovo -> endereco.cidade, cidade);
    pos = ordenar(inicio, nome);
    if (pos==0) {
        Inserir_Cliente_Comeco(inicio, nome, rg, telefone, email, rua, numero, cep, bairro, estado, cidade);
//        bkpcliente = fopen("bkpcliente.dat", "a+");
//        fwrite(nonovo, sizeof(cadastro_cliente), 1, bkpcliente);
//        fclose(bkpcliente);
    } else {
        int pos_aux=1;
        percorre = *inicio;
        while (pos_aux!=pos)
        {
            percorre = percorre -> prox;
            pos_aux++;
        }
        nonovo -> prox = percorre -> prox;
        percorre -> prox = nonovo;
        
    }
    return 0;
}





int Listar_Cliente (cadastro_cliente *inicio)
{
    
    if (inicio == NULL)
    {
        return 1;
    }
    while (inicio != NULL) {
        printf("%s",inicio->nome);
        inicio = inicio->prox;
    }
    printf("\n");
    return 0;
}




int ordenar (cadastro_cliente **inicio, char nome[51])
{
    int aux = 0;
    int pos;
    pos = 0;
    cadastro_cliente *percorre;
    percorre = *inicio;
    
    while ( aux < 1)
    {
        if (percorre == NULL){
            return pos;
        }
        aux = strcmp(percorre -> nome, nome);
        if (aux < 1){
            pos++;
        } else {
            return pos;
        }
        percorre = percorre -> prox;
        
        
    }
    return pos;
    ;
}



int Obter_Tamanho(cadastro_cliente **inicio)
{
    int tam;
    cadastro_cliente *percorre;
    tam = 0;
    if(*inicio != NULL)
    {
        percorre = *inicio;
        while (percorre != NULL)
        {
            (tam)++;
            percorre = percorre -> prox;
        }
    }
    else
    {
        tam = 0;
    }
    return tam;
}

int Remover_Cliente (cadastro_cliente **inicio, char nome[51]){
    int tam;
    tam = Obter_Tamanho(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_cliente *percorre, *percorreant;
    percorre = *inicio;
    percorreant = *inicio;
    
    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> nome, nome);
        if (aux==0){
            if (i==1){
                cadastro_cliente *deletar = percorre;
                *inicio = (*inicio) -> prox;
                free(deletar);
                return 0;
            }
            if (i>1 && i<=tam){
                cadastro_cliente *deletar = percorre;
                percorreant -> prox = percorre -> prox;
                free(deletar);
                return 0;
            }
        }
        
        percorreant = percorre;
        percorre = percorre -> prox;
    }
    return 0;
}
int Checar_Cliente (cadastro_cliente **inicio, char nome[51]){
    int tam;
    tam = Obter_Tamanho(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_cliente *percorre, *percorreant;
    percorre = *inicio;
    percorreant = *inicio;
    
    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> nome, nome);
        if (aux==0){
            printf("Cliente já cadastrado, dados cadastrais listados abaixo:\n");
            printf("\nNOME DO CLIENTE: %s\n", percorre -> nome);
            printf("RG DO CLIENTE: %s\n", percorre -> rg);
            printf("TELEFONE DO CLIENTE: %s\n", percorre -> telefone);
            printf("EMAIL DO CLIENTE: %s\n", percorre -> email);
            printf("*************ENDERECO DO CLIENTE*************\n");
            printf("RUA DO CLIENTE: %s\n", percorre -> endereco.rua);
            printf("NUMERO DO CLIENTE: %s\n", percorre -> endereco.numero);
            printf("CEP DO CLIENTE: %s\n", percorre -> endereco.cep);
            printf("BAIRRO DO CLIENTE: %s\n", percorre -> endereco.bairro);
            printf("ESTADO DO CLIENTE: %s\n", percorre -> endereco.estado);
            printf("CIDADE DO CLIENTE: %s\n", percorre -> endereco.cidade);
            printf("Deseja alterar o cadastro?\n1-Alterar Cadastro\n2-VOLTAR\n");
            int opc = 0;
            scanf("%d", &opc);
            if (opc==1) {
                Remover_Cliente(&ponteiro_clientes, nome);
                Gerar_Cadastro(&ponteiro_clientes, info);
                return 0;
            } else {
                return 0;
            }
        }
        percorreant = percorre;
        percorre = percorre -> prox;
    }
    
    printf("Usuário não cadastrado.\nDeseja cadastrar novo usuário?\n1-CADASTRAR\n2-SAIR\n");
    int opci = 0;
    scanf("%d", &opci);
    if (opci==1) {
        
        Gerar_Cadastro(&ponteiro_clientes, info);
        
        return 0;
    }
    
    return 0;
}



void carregarBinario(cadastro_cliente **inicio){
    bkpcliente = fopen("bkpcliente.dat","rb+");
    
    cadastro_cliente *percorre;
    
    //pegando o primeiro e guardando como inicio
    cadastro_cliente *noaux = (cadastro_cliente *) malloc(sizeof(cadastro_cliente));
    fread(noaux, sizeof(cadastro_cliente), 1, bkpcliente);
    *inicio = noaux;
    
    //iniciando o percorre para n„o perder a referencia do inicio
    percorre = *inicio;
    
    
    do{
        
        cadastro_cliente *noaux = (cadastro_cliente *) malloc(sizeof(cadastro_cliente));
        
        fread(noaux, sizeof(cadastro_cliente), 1, bkpcliente);
        
        percorre  -> prox = noaux;
        
        if(!feof(bkpcliente))
            percorre = percorre	-> prox;
        else
            percorre -> prox = NULL;
        
    }while(!feof(bkpcliente));
    
    
    fclose(bkpcliente);
    
}


void salvarBinario(cadastro_cliente **inicio){
    bkpcliente = fopen("bkpcliente.dat","wb");
    
    cadastro_cliente *percorre;
    
    percorre = *inicio;
    
    while(percorre != NULL){
        fwrite(percorre, sizeof(cadastro_cliente), 1, bkpcliente);
        percorre = percorre -> prox;
    }
    
    fclose(bkpcliente);
}


#endif
