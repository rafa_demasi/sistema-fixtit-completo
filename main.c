/*
 * File:   main.c
 * Author: Luique
 *
 * Created on 4 de Novembro de 2014, 21:10
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cadastrofuncionario.h"
#include "cadastrocliente.h"
#include "criarOS.h"


int main (){
    
    char nome_busca[51];
    char nome_busca_func[51];
    int op, opp;  /* receber a opcao do usuario */
    Inicializar_Cliente(&ponteiro_clientes);
    Inicializar_Func(&ponteiro_func);
    Inicializar_OS(&ponteiro_os);
    
    
    carregarBinario(&ponteiro_clientes);
    carregarBinario_Func(&ponteiro_func);
    carregarBinario_OS(&ponteiro_os);
    do{
        
        printf("*************MENU PRINCIPAL*************\nDIGITE A OPCAO DESEJADA:\n1-GERENCIAR CLIENTES \n2-GERENCIAR FUNCIONARIOS\n3-GERENCIAR 0.S.\n8-FECHAR PROGRAMA E SALVAR\n");
        scanf("%d", &opp);
        
        switch (opp) {
            case 1:
                do{
                    
                    printf("*************MENU DE CLIENTES*************\nDIGITE A OPCAO DESEJADA:\n1-CADASTRAR NOVO CLIENTE \n2-LISTAR CADASTROS\n3-REMOVER CADASTRO\n4-CHECAR CADASTRO\n8-VOLTAR AO MENU PRINCIPAL\n");
                    scanf("%d", &op);
                    
                    
                    switch(op) {
                        case 1:
                            Gerar_Cadastro(&ponteiro_clientes, info);
                            break;
                        case 2:
                            printf("*************LISTA DE CLIENTES*************\nNOME DO CLIENTE\n");
                            Listar_Cliente(ponteiro_clientes);
                            break;
                        case 3:
                            getchar();
                            printf("*************REMOCAO DE CLIENTES*************\nNOME DO CLIENTE:\n");
                            fgets(nome_busca, 51, stdin);
                            Remover_Cliente(&ponteiro_clientes, nome_busca);
                            printf("Usuário removido com sucesso.");
                            break;
                        case 4:
                            getchar();
                            printf("*************CHECAR CADASTRO DE CLIENTES*************\nNOME DO CLIENTE:\n");
                            fgets(nome_busca, 51, stdin);
                            Checar_Cliente(&ponteiro_clientes, nome_busca);
                            break;
                        case 8:
                            
                            break;
                            
                        default: printf("\nOpcao nao valida\n\n");
                            
                    }
                    
                    getchar();
                }while ((op != 8));
                break;
            case 2:
                do{
                    
                 
                    printf("*************MENU DE FUNCIONÁRIOS*************\nDIGITE A OPCAO DESEJADA:\n1-CADASTRAR NOVO FUNCIONÁRIO \n2-LISTAR FUNCIONÁRIOS\n3-REMOVER FUNCIONÁRIO\n4-CHECAR FUNCIONÁRIOS CADASTRADOS\n8-VOLTAR AO MENU PRINCIPAL\n");
                    scanf("%d", &op);
                    
                    
                    switch(op) {
                        case 1:
                            Gerar_Cadastro_Func(&ponteiro_func, info_func);
                            break;
                        case 2:
                            printf("*************LISTA DE FUNCIONARIOS*************\nNOME DO FUNCIONARIOS\n");
                            Listar_Func(ponteiro_func);
                            break;
                        case 3:
                            getchar();
                            printf("*************REMOCAO DE FUNCIONARIOS*************\nNOME DO FUNCIONARIOS:\n");
                            fgets(nome_busca_func, 51, stdin);
                            Remover_Func(&ponteiro_func, nome_busca_func);
                            printf("Usuário removido com sucesso.");
                            break;
                        case 4:
                            getchar();
                            printf("*************CHECAR CADASTRO DE FUNCIONARIOS*************\nNOME DO FUNCIONARIOS:\n");
                            fgets(nome_busca_func, 51, stdin);
                            Checar_Func(&ponteiro_func, nome_busca_func);
                            break;
                        case 8: break;
                            
                        default: printf("\nOpção Inválida.\n\n");
                            
                    }
                    
                    getchar();
                }while ((op != 8));
                
                break;
            case 3:
                do{
                    getchar();
                    
                    printf("*************ORDENS DE SERVIÇO*************\nDIGITE A OPCAO DESEJADA:\n1-ADICIONAR NOVA ORDEM\n2-LISTAR ORDENS\n3-REMOVER ORDEM\n4-CHECAR ORDENS\n8-VOLTAR AO MENU PRINCIPAL\n");
                    scanf("%d", &op);
                    
                    
                    switch(op) {
                        case 1:
                            Gerar_Cadastro_OS(&ponteiro_os, info_os, &ponteiro_clientes);
                            break;
                        case 2:
                            printf("*************LISTA DE ORDENS DE SERVIÇO*************\n");
                            Listar_OS(ponteiro_os);
                            break;
                        case 3:
                            getchar();
                            char nome_busca_os[51];
                            printf("*************REMOCAO DE ORDENS DE SERVIÇO*************\nNOME:\n");
                            fgets(nome_busca_os, 51, stdin);
                            Remover_OS(&ponteiro_os, nome_busca_os);
                            printf("Ordem de servico removida com sucesso.");
                            break;
                        case 4:
                            getchar();
                            printf("*************CHECAR ORDENS DE SERVIÇO*************\nNOME CLIENTE:\n");
                            fgets(nome_busca_os, 51, stdin);
                            Checar_OS(&ponteiro_os, nome_busca_os);
                            break;
                        case 8: break;
                            
                        default: printf("\nOpção Inválida.\n\n");
                            
                    }
                    
                    getchar();    
                }while ((op != 8));
                
                break;
            case 8:
                salvarBinario(&ponteiro_clientes);
                salvarBinario_Func(&ponteiro_func);
                salvarBinario_OS(&ponteiro_os);
                break;
            default: printf("\nOpção Inválida.\n\n");
        }
        
    } while ((opp!= 8));
    return (0);
}
