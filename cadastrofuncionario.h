//
//  Header.h
//  Sistema FixIt
//
//  Created by Luis Henrique Baster Massud on 07/11/14.
//  Copyright (c) 2014 Luique Works. All rights reserved.
//

#ifndef Sistema_FixIt_Header3_h
#define Sistema_FixIt_Header3_h

FILE *bkpfunc;

typedef struct tipo_func cadastro_func;
struct tipo_func {
    char nome[51];
    char rg[51];
    char telefone[51];
    char email[51];
    struct tipo_func *prox_func;
};

int numero_funcionario = 1;
cadastro_func *ponteiro_func;
cadastro_func info_func;

int Inicializar_Func (cadastro_func **inicio);
int Inserir_Func_Comeco (cadastro_func **inicio, char nome[51], char rg[51], char telefone[51], char email[51]);
int Inserir_Func_Meio (cadastro_func **inicio, char nome[51], char rg[51], char telefone[51], char email[51]);
int Inserir_Func_Fim(cadastro_func **inicio, char nome[51], char rg[51], char telefone[51], char email[51]);
int Listar_Func (cadastro_func *inicio);
int ordenar_Func (cadastro_func **inicio, char nome[51]);
int Obter_Tamanho_Func(cadastro_func **inicio);
int Remover_Func (cadastro_func **inicio, char nome[51]);
int Checar_Func (cadastro_func **inicio, char nome[51]);
int Gerar_Cadastro_Func (cadastro_func **inicio, cadastro_func info_func);
void salvarBinario_Func(cadastro_func **inicio);
void carregarBinario_Func(cadastro_func **inicio);



int Gerar_Cadastro_Func (cadastro_func **inicio, cadastro_func info_func){
    
    printf("*************CADASTRO DO FUNCIONÁRIO*************\nNOME DO FUNCIONÁRIO\n");
    getchar();
    fgets(info_func.nome, 51 , stdin);
    printf("\nRG DO FUNCIONARIO\n");
    fgets(info_func.rg, 51 , stdin);
    printf("\nTELEFONE DO FUNCIONARIO\n");
    fgets(info_func.telefone, 51, stdin);
    printf("\nEMAIL DO FUNCIONARIO\n");
    fgets(info_func.email, 51, stdin);
    numero_funcionario++;
    info_func.prox_func = NULL;
    Inserir_Func_Meio(&ponteiro_func, info_func.nome, info_func.rg, info_func.telefone, info_func.email);
    
    return 0;
}



int Inicializar_Func (cadastro_func **inicio) {
    
    *inicio= NULL;
    return 0;
}





int Inserir_Func_Comeco (cadastro_func **inicio, char nome[51], char rg[51], char telefone[51], char email[51]){
    
    cadastro_func *nonovo = NULL;
    nonovo = (cadastro_func *) malloc(sizeof(cadastro_func));
    strcpy(nonovo -> nome, nome);
    strcpy(nonovo -> rg, rg);
    strcpy(nonovo -> telefone, telefone);
    strcpy(nonovo -> email, email);
    if (*inicio==NULL)
    {
        nonovo -> prox_func = NULL;
        *inicio = nonovo;
    } else {
        nonovo -> prox_func = *inicio;
        *inicio = nonovo;
    }
    return 0;
}








int Inserir_Func_Meio (cadastro_func **inicio, char nome[51], char rg[51], char telefone[51], char email[51])
{
    int pos;
    int tam;
    tam = Obter_Tamanho_Func(inicio);
    cadastro_func *nonovo, *percorre;
    
   
    nonovo = (cadastro_func *)malloc(sizeof(cadastro_func));
    strcpy(nonovo -> nome, nome);
    strcpy(nonovo -> rg, rg);
    strcpy(nonovo -> telefone, telefone);
    strcpy(nonovo -> email, email);
    pos = ordenar_Func(inicio, nome);
    
    if (pos==0)
    {
        Inserir_Func_Comeco(inicio, nome, rg, telefone, email);
    }
    else
    {   if (pos == tam+1)
    {
        Inserir_Func_Fim(inicio, nome, rg, telefone, email);
    }
    else {
        int pos_aux=1;
        percorre = *inicio;
        while (pos_aux!=pos)
        {
            percorre = percorre -> prox_func;
            pos_aux++;
        }
        nonovo -> prox_func = percorre -> prox_func;
        percorre -> prox_func = nonovo;
    }
    }
    return 0;
}



int Inserir_Func_Fim(cadastro_func **inicio, char nome[51], char rg[51], char telefone[51], char email[51])
{
    cadastro_func *no_novo, *percorre;
    
    
    no_novo = (cadastro_func *) malloc(sizeof(cadastro_func));
    
    no_novo -> prox_func = NULL;
    
    if (*inicio==NULL)
    {
        *inicio = no_novo;
    }
    else {
        percorre = *inicio;
        while (percorre->prox_func != NULL)
        {
            percorre = percorre -> prox_func;
        }
        percorre->prox_func = no_novo;
    }
    return 0;
}




int Listar_Func (cadastro_func *inicio)
{
    
    if (inicio == NULL)
    {
        return 1;
    }
    while (inicio != NULL) {
        printf("%s",inicio->nome);
        inicio = inicio->prox_func;
    }
    printf("\n");
    return 0;
}




int ordenar_Func (cadastro_func **inicio, char nome[51])
{
    int aux = 0;
    int pos;
    pos = 0;
    cadastro_func *percorre;
    percorre = *inicio;
    
    while ( aux < 1)
    {
        if (percorre == NULL){
            return pos;
        }
        aux = strcmp(percorre -> nome, nome);
        if (aux < 1){
            pos++;
        } else {
            return pos;
        }
        percorre = percorre -> prox_func;
        
        
    }
    return pos;
    ;
}



int Obter_Tamanho_Func(cadastro_func **inicio)
{
    int tam;
    cadastro_func *percorre;
    tam = 0;
    if(*inicio != NULL)
    {
        percorre = *inicio;
        while (percorre != NULL)
        {
            (tam)++;
            percorre = percorre -> prox_func;
        }
    }
    else
    {
        tam = 0;
    }
    return tam;
}

int Remover_Func (cadastro_func **inicio, char nome[51]){
    int tam;
    tam = Obter_Tamanho_Func(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_func *percorre, *percorreant;
    percorre = *inicio;
    percorreant = *inicio;
    
    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> nome, nome);
        if (aux==0){
            if (i==1){
                cadastro_func *deletar = percorre;
                *inicio = (*inicio) -> prox_func;
                free(deletar);
                return 0;
            }
            if (i>1 && i<=tam){
                cadastro_func *deletar = percorre;
                percorreant -> prox_func = percorre -> prox_func;
                free(deletar);
                return 0;
            }
        }
        
        percorreant = percorre;
        percorre = percorre -> prox_func;
    }
    return 0;
}


int Checar_Func (cadastro_func **inicio, char nome[51]){
    int tam;
    tam = Obter_Tamanho_Func(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_func *percorre, *percorreant;
    percorre = *inicio;
    percorreant = *inicio;
    
    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> nome, nome);
        if (aux==0){
            printf("Cliente já cadastrado, dados cadastrais listados abaixo:\n");
            printf("\nNOME DO CLIENTE: %s\n", percorre -> nome);
            printf("RG DO CLIENTE: %s\n", percorre -> rg);
            printf("TELEFONE DO CLIENTE: %s\n", percorre -> telefone);
            printf("EMAIL DO CLIENTE: %s\n", percorre -> email);
            printf("Deseja alterar o cadastro?\n1-Alterar Cadastro\n2-Sair\n");
            int opc = 0;
            scanf("%d", &opc);
            if (opc==1) {
                Remover_Func(&ponteiro_func, nome);
                Gerar_Cadastro_Func(&ponteiro_func, info_func);
                return 0;
            } else {
                return 0;
            }
        }
        percorreant = percorre;
        percorre = percorre -> prox_func;
    }
    
    printf("Usuário não cadastrado.\nDeseja cadastrar novo usuário?\n1-CADASTRAR\n2-SAIR\n");
    int opci = 0;
    scanf("%d", &opci);
    if (opci==1) {
        
        Remover_Func(&ponteiro_func, nome);
        Gerar_Cadastro_Func(&ponteiro_func, info_func);
        
        return 0;
    }
    
    return 0;
}


void carregarBinario_Func(cadastro_func **inicio){
    bkpfunc = fopen("bkpfunc.dat","rb+");
    
    cadastro_func *percorre;
    
    //pegando o primeiro e guardando como inicio
    cadastro_func *noaux = (cadastro_func *) malloc(sizeof(cadastro_func));
    fread(noaux, sizeof(cadastro_func), 1, bkpfunc);
    *inicio = noaux;
    
    //iniciando o percorre para n„o perder a referencia do inicio
    percorre = *inicio;
    
    
    do{
        
        cadastro_func *noaux = (cadastro_func *) malloc(sizeof(cadastro_func));
        
        fread(noaux, sizeof(cadastro_func), 1, bkpfunc);
        
        percorre  -> prox_func = noaux;
        
        if(!feof(bkpfunc))
            percorre = percorre	-> prox_func;
        else
            percorre -> prox_func = NULL;
        
    }while(!feof(bkpfunc));
    
    
    fclose(bkpfunc);
    
}


void salvarBinario_Func(cadastro_func **inicio){
    bkpfunc = fopen("bkpfunc.dat","wb");
    
    cadastro_func *percorre;
    
    percorre = *inicio;
    
    while(percorre != NULL){
        fwrite(percorre, sizeof(cadastro_func), 1, bkpfunc);
        percorre = percorre -> prox_func;
    }
    
    fclose(bkpfunc);
}

#endif
