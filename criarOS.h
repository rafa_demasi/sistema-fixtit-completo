//
//  Header.h
//  Sistema FixIt
//
//  Created by Luis Henrique Baster Massud on 07/11/14.
//  Copyright (c) 2014 Luique Works. All rights reserved.
//

#ifndef Sistema_FixIt_Header2_h
#define Sistema_FixIt_Header2_h

FILE *bkpos;

int numerodefunc;
typedef struct tipo_data cadastro_data;
struct tipo_data {
    char dia[3];
    char mes[3];
    char ano[3];
    char horario[5];
    int quantidade_func_disp;
};

typedef struct tipo_os cadastro_os;
struct tipo_os {
    int numero_os;
    char cliente_os[51];
    tipo_endereco endereco_os;
    char func_os[51];
    cadastro_data data_os;
    char status_os[51];
    char comentario_os[600];
    struct tipo_os *prox_os;
};


cadastro_os *ponteiro_os;
cadastro_os info_os;
char numero_os_count = 0;

int Inicializar_OS (cadastro_os **inicio);
int Inserir_OS_Fim(cadastro_os **inicio, cadastro_os info_os);
int Listar_OS (cadastro_os *inicio);
int Obter_Tamanho_OS(cadastro_os **inicio);
int Remover_OS (cadastro_os **inicio, char nome[51]);
int Checar_OS (cadastro_os **inicio, char nome[51]);
int Gerar_Cadastro_OS (cadastro_os **inicio, cadastro_os info_os, cadastro_cliente **ini);
cadastro_os CopiardadosEndereco (cadastro_cliente **inicio, cadastro_os info_os, char nome[51]);
int Checardata (cadastro_os **inicio, cadastro_os info_os);
void salvarBinario_OS(cadastro_os **inicio);
void carregarBinario_OS(cadastro_os **inicio);


int Gerar_Cadastro_OS (cadastro_os **inicio, cadastro_os info_os, cadastro_cliente **inicio_cliente){
    printf("*************CADASTRO DE OS*************\nNOME DO CLIENTE\n");
    getchar();
    fgets(info_os.cliente_os, 51 , stdin);
    Checar_Cliente(&ponteiro_clientes, info_os.cliente_os);
    info_os = CopiardadosEndereco(&ponteiro_clientes, info_os, info_os.cliente_os);
    info_os.data_os.quantidade_func_disp = Obter_Tamanho_Func(&ponteiro_func);
    int menu = 0;
    do{
        getchar();
        printf("\nDigite o dia para a O.S. - (dois digitos - DD)\n");
        fgets(info_os.data_os.dia, 3, stdin);
        getchar();
        printf("\nDigite o mes para a O.S. - (dois digitos - MM)\n");
        fgets(info_os.data_os.mes, 3, stdin);
        getchar();
        printf("\nDigite o ano para a O.S. - (dois digitos - AA)\n");
        fgets(info_os.data_os.ano, 3, stdin);
        getchar();
        printf("\nDigite o horario do agendamento - (hh:mm)\n");
        fgets(info_os.data_os.horario, 3, stdin);
        getchar();
        int dataok = Checardata(&ponteiro_os, info_os);
        if (dataok == 0 || (ponteiro_os == NULL && info_os.data_os.quantidade_func_disp > 0)) {
            printf("\nData disponível, O.S. agendada.\n");
            info_os.data_os.quantidade_func_disp--;
            menu = 3;
            
        } else {
            printf("Data indisponível, verifique outras datas.");
        };
        getchar();
    }while ((menu != 3));
    strcpy(info_os.status_os, "Em andamento");
    printf("\nDigite um breve resumo do problema:\n");
    fgets(info_os.comentario_os, 600, stdin);
    menu=9;
    getchar();
    
    numero_os_count++;
    info_os.numero_os = numero_os_count;
    info_os.prox_os = NULL;
    Inserir_OS_Fim(&ponteiro_os, info_os);
    
    
    return 0;
}

int Checardata (cadastro_os **inicio, cadastro_os info_os){
    int tam;
    tam = Obter_Tamanho_OS(inicio);
    int auxdia = 1;
    int auxmes = 1;
    int auxano = 1;
    int pos, i;
    pos = 0;
    cadastro_os *percorre;
    percorre = *inicio;
    
    
    for (i=1; i <= tam; i++){
        auxdia = strcmp(percorre -> data_os.dia, info_os.data_os.dia);
        auxmes = strcmp(percorre -> data_os.mes, info_os.data_os.mes);
        auxano = strcmp(percorre -> data_os.ano, info_os.data_os.ano);
        if (auxdia==0 && auxmes==0 && auxano==0 && info_os.data_os.quantidade_func_disp > 0){
            return 0;
        } else if (info_os.data_os.quantidade_func_disp > 0){
            return 0;
        }
        percorre = percorre -> prox_os;
    }
   
    return 1;
}



cadastro_os CopiardadosEndereco (cadastro_cliente **inicio, cadastro_os info_os, char nome[51]){
    strcpy(info_os.cliente_os, nome);
    int tam;
    tam = Obter_Tamanho(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_cliente *percorre;
    percorre = *inicio;
    
    
    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> nome, nome);
        if (aux==0){
            strcpy(info_os.cliente_os, nome);
            strcpy(info_os.endereco_os.rua, percorre->endereco.rua);
            strcpy(info_os.endereco_os.numero, percorre->endereco.numero);
            strcpy(info_os.endereco_os.cep, percorre->endereco.cep);
            strcpy(info_os.endereco_os.bairro, percorre->endereco.bairro);
            strcpy(info_os.endereco_os.estado, percorre->endereco.estado);
            strcpy(info_os.endereco_os.cidade, percorre->endereco.cidade);
            
        }
    }
    return info_os;
}

int Inicializar_OS (cadastro_os **inicio) {
    
    *inicio= NULL;
    return 0; /* sem erro */
}






int Inserir_OS_Fim(cadastro_os **inicio, cadastro_os info_os)
{
    cadastro_os *nonovo, *percorre;
    
    /* Criacao do novo no - Alocação de memoria */
    nonovo = (cadastro_os *) malloc(sizeof(cadastro_os));
    strcpy(nonovo -> cliente_os, info_os.cliente_os);
    strcpy(nonovo -> func_os, info_os.func_os);
    strcpy(nonovo -> status_os, info_os.status_os);
    strcpy(nonovo -> comentario_os, info_os.comentario_os);
    strcpy(nonovo -> endereco_os.rua, info_os.endereco_os.rua);
    strcpy(nonovo -> endereco_os.numero, info_os.endereco_os.numero);
    strcpy(nonovo -> endereco_os.cep, info_os.endereco_os.cep);
    strcpy(nonovo -> endereco_os.bairro, info_os.endereco_os.bairro);
    strcpy(nonovo -> endereco_os.estado, info_os.endereco_os.estado);
    strcpy(nonovo -> endereco_os.cidade, info_os.endereco_os.cidade);
    strcpy(nonovo -> data_os.dia, info_os.data_os.dia);
    strcpy(nonovo -> data_os.mes, info_os.data_os.mes);
    strcpy(nonovo -> data_os.ano, info_os.data_os.ano);
    strcpy(nonovo -> data_os.horario, info_os.data_os.horario);
    nonovo -> data_os.quantidade_func_disp = info_os.data_os.quantidade_func_disp;
    nonovo -> numero_os = info_os.numero_os;
    nonovo -> prox_os = NULL;
    
    if (*inicio==NULL)
    {
        *inicio = nonovo;
    }
    else {
        percorre = *inicio;
        while (percorre->prox_os != NULL)
        {
            percorre = percorre -> prox_os;
        }
        percorre->prox_os = nonovo;
    }
    return 0;
}




int Listar_OS (cadastro_os *inicio)
{
    
    if (inicio == NULL)
    {
        return 1;
    }
    while (inicio != NULL) {
        printf("Cliente: %s",inicio->cliente_os);
        printf("Numero da OS: %d\n", inicio->numero_os);
        inicio = inicio->prox_os;
    }
    printf("\n");
    return 0;
}







int Obter_Tamanho_OS(cadastro_os **inicio)
{
    int tam;
    cadastro_os *percorre;
    tam = 0;
    if(*inicio != NULL)
    {
        percorre = *inicio;
        while (percorre != NULL)
        {
            (tam)++;
            percorre = percorre -> prox_os;
        }
    }
    else
    {
        tam = 0;
    }
    return tam;
}




int Remover_OS (cadastro_os **inicio, char nome[51]){
    int tam;
    tam = Obter_Tamanho_OS(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_os *percorre, *percorreant;
    percorre = *inicio;
    percorreant = *inicio;
    
    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> cliente_os, nome);
        if (aux==0){
            if (i==1){
                cadastro_os *deletar = percorre;
                *inicio = (*inicio) -> prox_os;
                free(deletar);
                return 0;
            }
            if (i>1 && i<=tam){
                cadastro_os *deletar = percorre;
                percorreant -> prox_os = percorre -> prox_os;
                free(deletar);
                return 0;
            }
        }
        
        percorreant = percorre;
        percorre = percorre -> prox_os;
    }
    return 0;
}





int Checar_OS (cadastro_os **inicio, char nome[51]){
    int tam;
    tam = Obter_Tamanho_OS(inicio);
    int aux = 1;
    int pos, i;
    pos = 0;
    cadastro_os *percorre, *percorreant;
    percorre = *inicio;
    percorreant = *inicio;

    for (i=1; i <= tam; i++){
        aux = strcmp(percorre -> cliente_os, nome);
        printf("Numeros das O.S. deste cliente estão listadas abaixo:\n");
        if (aux==0){

            printf("\nNOME DO CLIENTE: %s\n", percorre -> cliente_os);
            printf("NÚMERO DAS O.S.: %d\n", percorre -> numero_os);
            printf("DATA DA OS: %s/%s/%s\n", percorre -> data_os.dia, percorre -> data_os.mes, percorre -> data_os.ano);
            
            printf("HORARIO DA OS: %s\n", percorre -> data_os.horario);
            
            printf("Deseja alterar o cadastro?\n1-Alterar Cadastro\n2-Sair\n");
            int opc = 0;
            scanf("%d", &opc);
            if (opc==1) {
                Remover_OS(&ponteiro_os, nome);
                Gerar_Cadastro_OS(&ponteiro_os, info_os, &ponteiro_clientes);
                return 0;
            } else {
                return 0;
            }
        }
        percorreant = percorre;
        percorre = percorre -> prox_os;
    }

    printf("Usuário não cadastrado ou usuario sem OS cadastrada.\nDeseja cadastrar novo usuário?\n1-CADASTRAR\n2-SAIR\n");
    int opci = 0;
    scanf("%d", &opci);
    if (opci==1) {

        Remover_OS(&ponteiro_os, nome);
        Gerar_Cadastro_OS(&ponteiro_os, info_os, &ponteiro_clientes);

        return 0;
    }

    return 0;
}

void carregarBinario_OS(cadastro_os **inicio){
    bkpos = fopen("bkpos.dat","rb+");
    
    cadastro_os *percorre;
    
    //pegando o primeiro e guardando como inicio
    cadastro_os *noaux = (cadastro_os *) malloc(sizeof(cadastro_os));
    fread(noaux, sizeof(cadastro_os), 1, bkpos);
    *inicio = noaux;
    
    //iniciando o percorre para n„o perder a referencia do inicio
    percorre = *inicio;
    
    
    do{
        
        cadastro_os *noaux = (cadastro_os *) malloc(sizeof(cadastro_os));
        
        fread(noaux, sizeof(cadastro_os), 1, bkpos);
        
        percorre  -> prox_os = noaux;
        
        if(!feof(bkpos))
            percorre = percorre	-> prox_os;
        else
            percorre -> prox_os = NULL;
        
    }while(!feof(bkpos));
    
    
    fclose(bkpos);
    
}


void salvarBinario_OS(cadastro_os **inicio){
    bkpos = fopen("bkpos.dat","wb");
    
    cadastro_os *percorre;
    
    percorre = *inicio;
    
    while(percorre != NULL){
        fwrite(percorre, sizeof(cadastro_os), 1, bkpos);
        percorre = percorre -> prox_os;
    }
    
    fclose(bkpos);
}


#endif